dokumentace bude obsahovat součástky, postup a konečné vytvoření produktu

## MoodLamp projekt
**Cíl:** 
* Vytvořit aplikaci pro řízení světla ve formě lampy s možností nastavení jasnosti a měnění barev pomocí RGB palety a efektů

**Potřebné součástky**
* WeMos D1 mini Pro
* TP4056 USB Type C (na nabíjení baterky)
* Tlačítko
* 18650 Li-On baterie
* WS2812B LED pásek 
* 1000uF Kapacitor
* DHT11 senzor na měření teploty a vlhkosti
* dráty
* předem vytisknuté tělo MoodLampy

**Postup**
* Prvním krokem bylo vytisknutí pomocí 3D tiskárny těla pro MoodLampu
* Pokračovalo to testováním součástek a zjišťováním jejich funkčnosti
* Prvně byl otestován RGB LED pásek, se kterým nastaly menší komplikace, které prodloužili celé zhotovení projektu
* Jako další přišlo na řadu otestování senzoru DHT11 jednoduchým kódem, který vypisoval data ze senzoru na konzoli
* Následně nastal čas na spájení všech součástek na WeMos D1 mini Pro a opět testování správnosti napájení součástek
* Poté byl nahrán testovací kód na ověření funkčnosti všech součástek
* finálním krokem bylo vytvoření schématu zapojení v aplikaci Fritzing

**Vytvoření rozhraní pro ovládání MoodLampy**
* Rozhraní bylo vytvořené v Node-Redu pro jednoduché ovládání MoodLampy připojené do sítě
* V Node-Redu muselo být vytvořené propojení s WeMosem pomocí MQTT
* Nahráno bylo také OTA pro odesílaní aktualizace softwaru přes WiFi
* V Node-Redu byla vytvořena všechna potřebná propojení, aby projekt splňoval funkčnost a zadané požadavky od vyučujícího
* Vše se poté pomocí UI v Node-Redu dalo zobrazovat na stránku a měnit barvy a efekty vytvořené MoodLampy

**Výsledek**
* Celý tento výrobek mi zabral na výsledné zkompletování něco okolo 7 hodin čistého času 
* Bohužel, jak jsem již zmiňoval nastaly i lehké komplikace při testování funkčnosti RGB LED pásku, který, jak jsem zjistil, nefungoval od půlky celého pásku, takže jsem pásek musel o polovinu zkrátit
* Další závažný problém byl nefunkčí WeMos D1 mini Pro, takže tento problém prodloužil zhotovení produktu o necelé dva týdny
* Nakonec celý projekt byl i s menšími komplikacemi zhotoven podle požadavků vyučujícího a úspěšně odevzdán a připraven k prezentaci 