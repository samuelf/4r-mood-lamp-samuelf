# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | 7 hodin                         |
| odkud jsem čerpal inspiraci                   | https://www.instructables.com/RGB-Lamp-WiFi/                           |
| odkaz na video                                | https://youtu.be/qroPeuH1DmI                     |
| jak se mi to podařilo rozplánovat             | Naplánováno jsem měl dobře, ale vznikli menší potíže se součástkami |
| proč jsem zvolil tento design                 | Jednoduché a dá se s tím i dále pracovat |
| zapojení                                      | https://gitlab.spseplzen.cz/samuelf/4r-mood-lamp-samuelf/-/blob/main/dokumentace/fotky/IMG_3625.jpg          |
| z jakých součástí se zapojení skládá          | WeMos D1 mini Pro, TP4056 USB Type C (na nabíjení baterky), Tlačítko, 18650 Li-On baterie, WS2812B LED pásek, 1000uF,Kapacitor, DHT11 senzor na měření teploty a vlhkosti, dráty, předem vytisknuté tělo MoodLampy                                |
| realizace                                     | https://gitlab.spseplzen.cz/samuelf/4r-mood-lamp-samuelf/-/blob/main/dokumentace/fotky/IMG_3647.jpg |
| UI                                            | https://gitlab.spseplzen.cz/samuelf/4r-mood-lamp-samuelf/-/blob/main/dokumentace/fotky/UI.png                |
| co se mi povedlo                              | uspořádat drátky do spodu lampy |
| co se mi nepovedlo/příště bych udělal/a jinak | Koupil bych jiný RGB pásek, s tímto byli zbytečné problémy                                 |
| zhodnocení celé tvorby (návrh známky)         | 9/10 známka: 1                  |